import yaml
import asyncio
from aiohttp import ClientSession
from pathlib import Path
from datetime import datetime
from functools import partial

from pprint import pprint


async def main():
    headers = [("Content", "application/json"), ("Accept", "application/json")]

    async with ClientSession(headers=headers) as session:
        token, site_id = await get_token(session)
        origin = config["api"]["origin"]
        version = config["api"]["version"]

    async with ClientSession(headers=[*headers, ("X-Tableau-Auth", token)]) as session:
        # await query_db_tables(session, token, site_id, origin, version)

        def fn(method, path, json=None):
            url = f"{origin}/api/{version}/sites/{site_id}{path}"
            return session.request(method, url, json=json)

        """ list projects
        async with fn('GET', '/projects') as res:
            body = await res.json()
            pprint(body)

        """

        async with fn("GET", "/datasources") as res:
            body = await res.json()

            print(body)

            datasource_id = next(
                (
                    o["id"]
                    for o in body["datasources"]["datasource"]
                    if o["name"] == "InvoiceToolCommunity"
                ),
                None,
            )

            assert datasource_id is not None, "failed to retrieve datasource"

        async with fn("POST", f"/datasources/{datasource_id}/refresh", {}) as res:
            body = await res.json()
            job_id = body["job"]["id"]

            assert job_id is not None, "failed to retrieve job after refresh"

        async with fn("GET", f"/jobs/{job_id}") as res:
            body = await res.json()
            pprint(body)

        dt = datetime.now()
        dt = datetime(*dt.timetuple()[:3])
        dt = dt.strftime("%Y-%m-%dT%H:%M:%SZ")

        get_extract_refreshes = partial(
            fn, "GET", f"/jobs?filter=createdAt:gt:{dt},jobType:eq:refresh_extracts"
        )

        for _ in range(10):
            async with get_extract_refreshes() as res:
                body = await res.json()

                background_jobs = [
                    o
                    for o in body["backgroundJobs"]["backgroundJob"]
                    if o["status"] not in ("Success", "Failed")
                ]

                pprint(background_jobs)
            await asyncio.sleep(2)

        async with fn(
            "GET", f"/jobs?filter=createdAt:gt:{dt},jobType:eq:refresh_extracts"
        ) as res:
            body = await res.json()
            for job in body["backgroundJobs"]["backgroundJob"]:
                if job["status"] != "Success":
                    pprint(job)


async def query_db_tables(session: ClientSession, token, site_id, origin, version):
    query = """
    query {
      databases (filter: {name: "community"}){
        name
        tables{
          name
        }
      }
    }
    """
    async with session.post(
        f"{origin}/api/metadata/graphql",
        json={"query": query},
        headers={"X-Tableau-Auth": token},
    ) as res:
        body = await res.json()
        pprint(body)


async def get_token(session):
    cached_token_path = Path.cwd() / "token"
    if cached_token_path.is_file():
        with open(cached_token_path, "r") as f:
            site_id, token = f.read().split(",")
            return token, site_id

    payload = (
        construct_token_payload if "token" in config else construct_password_payload
    )(config)
    origin = config["api"]["origin"]
    version = config["api"]["version"]

    async with session.post(
        f"{origin}/api/{version}/auth/signin",
        json=payload,
    ) as resp:
        if resp.status != 200:
            print(await resp.text())
            raise Exception("failed!")
        body = await resp.json()
        token = body["credentials"]["token"]
        site_id = body["credentials"]["site"]["id"]

    with open(cached_token_path, "w") as f:
        f.write(f"{site_id},{token}")

    return token, site_id


def construct_token_payload(config):
    return {
        "credentials": {
            "personalAccessTokenName": config["token_id"],
            "personalAccessTokenSecret": config["token"],
            "site": {
                "contentUrl": config["site"],
            },
        }
    }


def construct_password_payload():
    return {
        "credentials": {
            "name": email,
            "password": password,
            "site": {"contentUrl": site},
        }
    }


if __name__ == "__main__":
    config_file = Path.cwd() / "config.yaml"
    assert config_file.is_file(), "config file missing!"
    with open(config_file) as f:
        config = yaml.safe_load(f)
    asyncio.run(main())
